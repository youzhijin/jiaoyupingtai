import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

import baseUrl from 'config/index.js'
import timeFun from 'config/time.js'
App.mpType = 'app'

Vue.prototype.$url = baseUrl;

const app = new Vue({
    ...App
})
app.$mount()
